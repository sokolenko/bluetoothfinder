package yurasik.com.bluetoothfinder;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import io.realm.Realm;
import yurasik.com.bluetoothfinder.db.DBHandler;
import yurasik.com.bluetoothfinder.db.DeviceModel;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "YReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        StringBuilder sb = new StringBuilder();
        sb.append("Action: " + intent.getAction() + "\n");
        sb.append("URI: " + intent.toUri(Intent.URI_INTENT_SCHEME).toString() + "\n");
        String log = sb.toString();
        Log.d(TAG, "onReceive: " + intent.getAction());
        //bluetooth device found
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        showInformationDevice(context, intent.getAction(), device);
    }

    private void showInformationDevice(Context context, String message, BluetoothDevice device) {
        Log.d(TAG, message);
        //Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        if (device != null) {
            DBHandler.save(device);
            Realm realm = Realm.getDefaultInstance();
            DeviceModel deviceModel = realm
                    .where(DeviceModel.class)
                    .contains("address", device.getAddress())
                    .findFirst();
            if (deviceModel!=null && deviceModel.isActive()) {
                Toast.makeText(context, deviceModel.getName()+" on action: " + message,
                        Toast.LENGTH_LONG).show();
            }
            realm.close();
        }
    }
}
