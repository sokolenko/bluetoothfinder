package yurasik.com.bluetoothfinder.db;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Yurii on 10/22/17.
 */

public class Model extends RealmObject {

    private long time;
    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Model{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", time=" + time +
                '}';
    }
}
