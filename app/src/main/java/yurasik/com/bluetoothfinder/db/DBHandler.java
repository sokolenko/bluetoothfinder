package yurasik.com.bluetoothfinder.db;

import android.bluetooth.BluetoothDevice;

import javax.annotation.Nonnull;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Yurii on 10/22/17.
 */

public class DBHandler {
    public static void save(@Nonnull BluetoothDevice device) {

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Model model = realm.createObject(Model.class);
        model.setTime(System.currentTimeMillis());
        model.setName(device.getName());
        model.setAddress(device.getAddress());
        //model.setType(device.getType());
        realm.commitTransaction();
        realm.close();

    }

}
