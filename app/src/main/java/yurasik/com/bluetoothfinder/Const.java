package yurasik.com.bluetoothfinder;

/**
 * Created by Yurii on 10/15/17.
 */

public class Const {

    public static class REQUEST {
        public static final int ENABLE_BT = 1000;
    }

    public static class ACTION {
        public static final String START = "start";
        public static final String SCAN = "scan";
        public static final String DESTROY = "destroy";
    }
}
