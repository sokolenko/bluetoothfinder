package yurasik.com.bluetoothfinder;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

/**
 * Created by Yurii on 10/15/17.
 */

public class FinderService extends Service {

    BluetoothAdapter mBluetoothAdapter;
    public static boolean isStarted;

    protected Handler handler = new Handler();
    protected CheckRunnable checkRunnable = new CheckRunnable();

    class CheckRunnable implements Runnable {

        @Override
        public void run() {
            scan();
            handler.postDelayed(checkRunnable, TimeUnit.SECONDS.toMillis(40));
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {


        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            Timber.d("OnReceive action: " + action);

            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                //discovery starts, we can show progress dialog or perform other tasks
                Timber.d("ACTION_DISCOVERY_STARTED");
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                //discovery finishes, dismis progress dialog
                Timber.d("ACTION_DISCOVERY_FINISHED");
            } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                //bluetooth device found
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                showInformationDevice("Receiver:", device);
            } else if(BluetoothDevice.ACTION_UUID.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                Parcelable[] uuidExtra = intent.getParcelableArrayExtra(BluetoothDevice.EXTRA_UUID);
                for (int i=0; i<uuidExtra.length; i++) {
                    Timber.d("Device: " + device.getName() + ", " + device + ", Service: " + uuidExtra[i].toString());
                }
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Timber.d("Device does not support Bluetooth");
        } else {
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

            filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
            filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);

            registerReceiver(mReceiver, filter);
        }
    }

    /* public FinderService() {
        super("FinderService");

        *//*if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Const.REQUEST.ENABLE_BT);
        }*//*
    }*/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!=null && intent.getAction()!=null)
        switch (intent.getAction()) {
            case Const.ACTION.START:
                Timber.d("Service started...");
                isStarted = true;
                handler.post(checkRunnable);
                break;
            case Const.ACTION.SCAN:
                scan();
                break;
            case Const.ACTION.DESTROY:
                stopSelf();

        }
        return super.onStartCommand(intent, flags, startId);
    }

    protected void scan() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        BluetoothManager manager = getBaseContext().getSystemService(BluetoothManager.class);// with the argument


        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                showInformationDevice("Paired:", device);
                manager.getConnectionState(device, 0);
            }
        }
        mBluetoothAdapter.startDiscovery();

    }

    protected void showInformationDevice(String worker, BluetoothDevice device) {
        Timber.d(worker+ " found device " + device.getName()
                + " address: " + device.getAddress()
                + " bluetooth class: " + device.getBluetoothClass());

        Parcelable[] uuidExtra = device.getUuids();
        if (uuidExtra!=null)
            for (int i=0; i<uuidExtra.length; i++) {
                Timber.d("Device: " + device.getName() + ", " + device + ", Service: " + uuidExtra[i].toString());
            }
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(checkRunnable);
        mBluetoothAdapter.cancelDiscovery();
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
        isStarted = false;
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
