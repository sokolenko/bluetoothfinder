package yurasik.com.bluetoothfinder.ui.devices;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import yurasik.com.bluetoothfinder.R;
import yurasik.com.bluetoothfinder.db.DeviceModel;

/**
 * Created by Yurii on 10/22/17.
 */

public class DevicesRealmAdapter extends RealmRecyclerViewAdapter<DeviceModel, DevicesRealmAdapter.DevicesHolder> {

    protected Realm realm;

    public DevicesRealmAdapter(@Nullable OrderedRealmCollection<DeviceModel> data) {
        super(data, true);
        setHasStableIds(true);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        realm.close();
        realm = null;
    }

    @Override
    public DevicesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_device, parent, false);
        return new DevicesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DevicesHolder holder, int position) {
        holder.data = getItem(position);
        holder.tvName.setText(holder.data.getName());
        holder.toggleActive.setChecked(holder.data.isActive());
    }

    class DevicesHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        ToggleButton toggleActive;
        public DeviceModel data;

        DevicesHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            toggleActive = view.findViewById(R.id.toggleDevice);
            toggleActive.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (data.isActive()==b) return;
                    realm.beginTransaction();
                    data.setActive(b);
                    //data = realm.copyToRealmOrUpdate(data);
                    realm.commitTransaction();
                }
            });
        }
    }

}