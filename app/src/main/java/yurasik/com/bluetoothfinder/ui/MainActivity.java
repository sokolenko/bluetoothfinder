package yurasik.com.bluetoothfinder.ui;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import timber.log.Timber;
import yurasik.com.bluetoothfinder.Const;
import yurasik.com.bluetoothfinder.FinderService;
import yurasik.com.bluetoothfinder.R;
import yurasik.com.bluetoothfinder.ui.devices.DevicesActivity;

@RuntimePermissions
public class MainActivity extends BaseActivity {

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @BindView(R.id.btnScan)
    protected Button btnScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FinderService.class);
                intent.setAction(Const.ACTION.SCAN);
                startService(intent);
            }
        });
        MainActivityPermissionsDispatcher.shotBluetoothWithPermissionCheck(this);
    }

    @OnClick(R.id.btnDevices)
    protected void clickToDevices() {
        MainActivityPermissionsDispatcher.startDevicesActivityWithPermissionCheck(this);
    }

    @NeedsPermission({Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION})
    protected void startDevicesActivity() {
        startActivity(new Intent(this, DevicesActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Timber.d("onResume");
        btnScan.setEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Timber.d("onPause");
        //btnScan.setEnabled(!FinderService.isStarted);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @NeedsPermission({Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION})
    void shotBluetooth() {
        if (!FinderService.isStarted) {
            Intent intent = new Intent(this, FinderService.class);
            intent.setAction(Const.ACTION.START);
            startService(intent);
            //btnScan.setEnabled(false);
        }
    }

    @Override
    protected void onDestroy() {
        if (FinderService.isStarted) {
            Intent intent = new Intent(this, FinderService.class);
            intent.setAction(Const.ACTION.DESTROY);
            startService(intent);
            btnScan.setEnabled(true);
        }
        super.onDestroy();
    }
}
