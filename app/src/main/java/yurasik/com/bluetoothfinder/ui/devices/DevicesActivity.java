package yurasik.com.bluetoothfinder.ui.devices;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;

import java.util.Set;

import butterknife.BindView;
import io.realm.Realm;
import timber.log.Timber;
import yurasik.com.bluetoothfinder.R;
import yurasik.com.bluetoothfinder.db.DeviceModel;
import yurasik.com.bluetoothfinder.ui.BaseActivity;

/**
 * Created by Yurii on 10/20/17.
 */

public class DevicesActivity extends BaseActivity {

    protected Realm realm;
    private DevicesRealmAdapter adapter;

    @BindView(R.id.recyclerView)
    protected RecyclerView recyclerView;

    @Override
    protected int getLayout() {
        return R.layout.activity_devices;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        setUpRecyclerView();
        updateDevices();
    }

    private void updateDevices() {
        BluetoothAdapter bluetoothAdapter
                = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices
                = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            realm.beginTransaction();
            for (BluetoothDevice device : pairedDevices) {
                String address = device.getAddress();
                DeviceModel deviceModel = realm
                        .where(DeviceModel.class)
                        .contains("address", address)
                        .findFirst();
                if (deviceModel==null) {
                    deviceModel = realm.createObject(DeviceModel.class);
                    deviceModel.setName(TextUtils.isEmpty(device.getName()) ? "No name" : device.getName());
                    deviceModel.setAddress(device.getAddress());
                    Timber.d("New device: " + deviceModel);
                }
            }
            realm.commitTransaction();
        }
    }


    private void setUpRecyclerView() {
        adapter = new DevicesRealmAdapter(realm.where(DeviceModel.class).findAll());
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        /*TouchHelperCallback touchHelperCallback = new TouchHelperCallback();
        ItemTouchHelper touchHelper = new ItemTouchHelper(touchHelperCallback);
        touchHelper.attachToRecyclerView(recyclerView);*/
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }
}
